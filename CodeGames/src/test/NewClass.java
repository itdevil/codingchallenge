/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author loc
 */
public class NewClass {

    public static void main(String[] args) {
        
        String input = "abc";
        char a [] = input.toCharArray();
        
        List<String> results = new ArrayList<>();
        for (int i = 1; i <= (1 << a.length) - 1; i++) {
            StringBuilder builder = new StringBuilder();
            for (int j = 0; j < a.length; j++) {
                if ((i & (1 << j)) != 0) {
                    builder.append(a[j]);
                }
            }
            results.add(builder.toString());
        }
        
        Collections.sort(results);
        System.out.println(results);
    }
}
