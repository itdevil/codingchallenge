/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author loc
 */
public class CountOcurencees {

    static public void sort(int arr[]) {
        int n = arr.length;

        for (int i = n / 2 - 1; i >= 0; i--) {
            heapify(arr, n, i);
        }

        for (int i = n - 1; i >= 0; i--) {
            // Move current root to end
            int temp = arr[0];
            arr[0] = arr[i];
            arr[i] = temp;
            heapify(arr, i, 0);
        }
    }

    static void heapify(int arr[], int n, int i) {
        int largest = i;  
        int l = 2 * i + 1;  
        int r = 2 * i + 2;  

        
        if (l < n && arr[l] > arr[largest]) {
            largest = l;
        }

        
        if (r < n && arr[r] > arr[largest]) {
            largest = r;
        }

        if (largest != i) {
            int swap = arr[i];
            arr[i] = arr[largest];
            arr[largest] = swap;

            heapify(arr, n, largest);
        }
    }

    static void printArray(int arr[]) {
        int n = arr.length;
        for (int i = 0; i < n; ++i) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    static public int countDuplicatedNumbers(int arr[]) {

        int count = 0;
        int countDup = 1;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] == arr[i + 1]) {
                countDup++;
                
            } else {
                if (countDup >= 2) {
                  
                    count++;
                }
                countDup = 1;

            }
        }

        if (countDup >= 2) {
            count++;
        }

        return count;
    }

    // Driver program
    public static void main(String args[]) {
        int arr[] = {12, 12, 4, 13, 1, 6, 1, 3, 8, 4, 5, 2, 2, 2, 3, 13};
        int n = arr.length;
        Arrays.sort(arr);
        printArray(arr);
        System.out.println("test.CountOcurencees.main() : " + countDuplicatedNumbers(arr));

    }
}
