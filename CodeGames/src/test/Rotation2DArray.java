/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author loc
 */
public class Rotation2DArray {

    public static Integer[] arrayLeftRotation(Integer[] array, int n, int k) {
        // copy array
        Integer temp[] = Arrays.copyOf(array, k);

        for (int i = 0; i < n; i++) {
            if (i + k >= n) {
                break;
            }
            array[i] = array[i + k];
        }

        int count = 0;
        for (int i = n - k; i < n; i++) {
            array[i] = temp[count++];
        }

        return array;
    }

    /*
    {1,2,3,4},
    {5,6,7,8},
    {9,1,2,3},
    {4,5,6,7}
     */
    public static List<Integer[]> convertMatrixToArrays(Integer[][] matrix, int n) {
        int numberOfArrays = n / 2;
        List<Integer[]> list = new ArrayList<>();
        for (int j = 0; j < numberOfArrays; j++) {
            // top
            int length_1 = (n - 2 * j) - 1;
            int length ;
            if( j == 0)
                length = (n - 2 * j) - 1;
            else
                length = (n - 2 * j);
            
            System.out.println("length : " + length);
            Integer array[] = new Integer[length_1 * 4];

            int count = 0;
            int top = j;
            for (int i = j; i < length; i++) {
                array[count++] = matrix[top][i];
                System.out.print(matrix[top][i]);
            }
                
            System.out.println();
            int right = length;
            for (int i = j; i < length; i++) {
                array[count++] = matrix[i][right];
                 System.out.print( matrix[i][right]);
            }
            
            System.out.println();
            int botton = length;
            for (int i = length; i > j; i--) {
                array[count++] = matrix[botton][i];      
                System.out.print(matrix[botton][i]);
            }
            
            System.out.println();
            int left = j;
            for (int i = length; i > j; i--) {
                array[count++] = matrix[i][left];
                System.out.print( matrix[i][left]);
            }

            list.add(array);
        }

        return list;
    }

    public static Integer[][] convertArraysToMatrix( int n, List<Integer[]> arrays) {
        int numberOfArrays = n / 2;
        Integer[][] matrix = new Integer[5][5];
        for (int j = 0; j < numberOfArrays; j++) {
            // top
            int length ;
            if( j == 0)
                length = (n - 2 * j) - 1;
            else
                length = (n - 2 * j);
            
            System.out.println("length : " + length);
            Integer array[] = arrays.get(j);

            int count = 0;
            int top = j;
            for (int i = j; i < length; i++) {
                matrix[top][i] = array[count++];
                System.out.print(matrix[top][i]);
            }
                
            System.out.println();
            int right = length;
            for (int i = j; i < length; i++) {
                matrix[i][right] = array[count++];
                System.out.print( matrix[i][right]);
            }
            
            System.out.println();
            int botton = length;
            for (int i = length; i > j; i--) {
                matrix[botton][i] = array[count++];      
                System.out.print(matrix[botton][i]);
            }
            
            System.out.println();
            int left = j;
            for (int i = length; i > j; i--) {
                matrix[i][left] = array[count++];
                System.out.print( matrix[i][left]);
            }

        }

        return matrix;
    }

    public static void main(String[] args) {

        Integer[][] matrix = new Integer[][]{
            {1, 2, 3, 4,5},
            {5, 6, 7, 8,5},
            {9, 1, 2, 3 ,5},
            {4, 5, 6, 7 ,5},
            {4, 5, 6, 7 ,5}};

        List<Integer[]> arrays = convertMatrixToArrays(matrix, 5);
        List<Integer[]> rotateArrays = convertMatrixToArrays(matrix, 5);
        
        arrays.forEach((array) -> {
//            System.out.println(Arrays.asList(array)); 
            rotateArrays.add(arrayLeftRotation(array,array.length,1));
        });
        
        rotateArrays.forEach((array) -> {
//            System.out.println(Arrays.asList(array)); 
        });
        
       
        Integer [][] rotateMatrix = convertArraysToMatrix(5,rotateArrays );
         System.out.println();
        for(int i = 0;i<5;i++){
            for(int j = 0;j<5;j++){
                System.out.print(rotateMatrix[i][j] + " "); 
            }
             System.out.println();
        }

    }

}
