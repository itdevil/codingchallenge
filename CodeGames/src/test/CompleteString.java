/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.BufferedReader;
import java.io.InputStreamReader;

//import for Scanner and other utility  classes
import java.util.*;


/**
 *
 * @author loc
 */
public class CompleteString {
     private static final char[] alphabet ="qwertyuioplkjhgfdsazxcvbnm".toCharArray() ;

    public static boolean isCompleteString(String text, String correctString){

        if(text.length() < alphabet.length){
            return false;
        }
        else{
            Character checkingChars [] = correctText(text);
            Arrays.sort(checkingChars);

            return Arrays.toString(checkingChars).equals(correctString);
        }

    }

    public static Character[] correctText(String text){
        char characters [] = text.toCharArray();
        Arrays.sort(characters);
        Set<Character> deDupChar = new HashSet<Character>();
        for(Character ch : characters){
            deDupChar.add(ch);
        }


        return deDupChar.toArray(new Character[deDupChar.size()]);

    }
    
    public static void main(String args[] ) throws Exception {
     
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = br.readLine();
        int N = Integer.parseInt(line);
        
//        Scanner s = new Scanner(System.in);
//        int N = s.nextInt();
        
        Arrays.sort(alphabet);
        String correctString = Arrays.toString(alphabet);
        for (int i = 0; i < N; i++) {
            
            String text = br.readLine();
            
            if(isCompleteString(text,correctString)){
                System.out.println("YES");
            }else{
                System.out.println("NO");
            }
        }
        
    }
}
