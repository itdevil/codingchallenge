/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author loc
 */
public class FindMIMEType {
    
    public static void findMIMEType(Map<String,String> mapMime, String [] filenames) {
        
        for(String name : filenames) {
            
            if (name.contains("."))
            {
                String extension = name.substring(name.lastIndexOf(".") + 1,name.length());
                // System.out.println(extension);
                String mimitype = mapMime.get(extension);
                if(mimitype != null) {
                 System.out.println(mimitype);
                }
                else{
                    System.out.println("UNKNOWN");
                }
            }else {
                System.out.println("UNKNOWN");
            }   
        }
    }

    public static void main(String args[]) throws FileNotFoundException {
        
        File file = new File("MIMETypeSample.dat");
        
        Scanner in = new Scanner(file);
        int N = in.nextInt(); // Number of elements which make up the association table.
        int Q = in.nextInt(); // Number Q of file names to be analyzed.
        
        Map<String,String> tableMIMEType = new HashMap<String,String>();
        
        
        for (int i = 0; i < N; i++) {
            String EXT = in.next(); // file extension
            String MT = in.next(); // MIME type.
            tableMIMEType.put(EXT.toLowerCase(),MT);
        }
        in.nextLine();
        
        String [] fileNames = new String[Q];
        
        for (int i = 0; i < Q; i++) {
            String FNAME = in.nextLine(); // One file name per line.
            fileNames[i] = FNAME.toLowerCase();
        }
        // For each of the Q filenames, display on a line the corresponding MIME type. If there is no corresponding type, then display UNKNOWN.
        findMIMEType (tableMIMEType,fileNames);
    }
}
