/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.Arrays;
/**
 *
 * @author loc
 */
public class ExtractSubArray {

    
    public static boolean isValidSubArray(Stack<Integer> sub){
        
        int sum  = 0;
        for(Integer element : sub){
            sum = sum + element;
        }
        
        return sum < 20;
      
    }
    
    public static List<Stack<Integer>> doDo() {
        final List<Integer> elements = Arrays.asList(9, 7, 6, 6, 4, 3, 4, 5, 3, 4, 3, 4, 1, 2); 
        final List<Stack<Integer>> subArrays = new ArrayList<>();
       
        Stack<Integer> sub = new Stack<>();
        int sum  = 0;
        for(Integer element : elements) {     
            sub.add(element);   
            sum = sum + element;
            if(sum >= 20){
//                remove top becaus eit make sum >= 20
                Integer e = sub.pop();
                subArrays.add(sub);
                sub = new Stack<>();
                sum = e;
                if (e < 20)
                    sub.add(e);
            } 
        }   
//        add last sub array
        if(!sub.isEmpty())
            subArrays.add(sub);
    
        return subArrays;

    }

    public static void main(String[] args) {
        List<Stack<Integer>> stackList = doDo();
        for (Stack<Integer> stack : stackList) {
            for (Integer element : stack) {
                System.out.print(element.toString() + " ");
            }
            System.out.println();
        }
    }

}
