/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author loc
 */
public class FindSubArray {
    
   
    
    
    public static List<Integer> findIndicesOfSubArray(int [] parent, int []child) {
        
        List<Integer> positions = new ArrayList<>();
        int maxIndex = parent.length - child.length + 1;
        for(int i = 0;i<maxIndex;i++){
            int countIndex = 0;
            for( int j = 0;j<child.length;j++){
                if(child[j] != parent[i + countIndex++]){
                    break;
                }
            }
            if( countIndex == child.length ){
                positions.add(i);
            }    
        }   
        return positions;
        
    }
    
    public static void main(String[] args) {
    
        int [] parent = new int[]{1,2,7,8,9,4,5,6,7,8,9};
        int [] child = new int[]{7,8,9};
        
        System.out.println(findIndicesOfSubArray(parent,child));  
    }
    
}
