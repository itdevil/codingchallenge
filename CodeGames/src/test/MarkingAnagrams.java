/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author loc
 */
public class MarkingAnagrams {
    
    public static int numberNeeded(String first, String second) {
      
        Set<String> letters = new HashSet<>();
        
        Map<String,Integer> firstMap = countCharacters(first, letters);
        Map<String,Integer> secondMap = countCharacters(second, letters);
        
        Integer number = 0;
        for (String letter : letters){
            
            Integer countLetterInFirst = firstMap.getOrDefault(letter, 0);
            Integer countLetterInSecond = secondMap.getOrDefault(letter,0);
            
            number = number + Math.abs(countLetterInFirst - countLetterInSecond);
           
        }
                
           return number;
    }
    
    public static Map<String,Integer> countCharacters(String text,Set<String> letters){
        
       Map<String,Integer> mapCharacter = new HashMap<>();
       for (Character c : text.toCharArray()){
           String letter = c.toString();
           letters.add(letter);
           if(mapCharacter.containsKey(letter)){
               int count = mapCharacter.get(letter) + 1;
               mapCharacter.put(letter, count);
           }else{
               mapCharacter.put(letter, 1);
           }
       }
       
       return mapCharacter;
        
    }
    
    public static void main(String[] args){
    
        String first = "cde";
        String second = "abc";
        
        System.out.println(numberNeeded(first, second));
    }
}
