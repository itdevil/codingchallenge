/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 *
 * @author loc
 */
public class BinaryConvert {
    
    public static String decimalToBinary(long decimalNumber){
       
        StringBuilder builder = new StringBuilder();
        
        long number = decimalNumber;
        while(true){
            long tmp = number / 2;
            builder.append((number % 2));
            number = tmp;
            
            if(tmp == 0)
            {
                break;
            }   
          
        }
        
        return builder.reverse().toString();        
        
    }
    
    
    public static void main(String []args) {
        
//        System.out.println(Long.MAX_VALUE);
        System.out.println(decimalToBinary(328));
        
        List<Integer> r = new ArrayList<>();
        
        r.clear();
        
 
    }
    
}
