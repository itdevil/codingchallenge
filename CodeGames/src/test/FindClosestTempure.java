/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.Scanner;

/**
 *
 * @author loc
 */
public class FindClosestTempure {
    
     public static int findClosestZero(int [] temps){
        
        int closestTempure = 0;
        int minDiffTemp = Math.abs(temps[0]);
        int diff = 0;
        for(int i = 0;i<temps.length;i++){
            diff = Math.abs(temps[i] - 0 );
            // System.out.println((diff));
           if( diff <= minDiffTemp && diff != 0) {   
               minDiffTemp = diff;
               
               if ( temps[i] < 0 && closestTempure == -temps[i]) 
                    closestTempure = -temps[i];
               else
                    closestTempure = temps[i];
               
            }
        }
        
        return closestTempure;
        
    }

    public static void main(String args[]) {
//        Scanner in = new Scanner(System.in);
//        int n = in.nextInt(); // the number of temperatures to analyse
//        if (in.hasNextLine()) {
//            in.nextLine();
//        }
//        String temps = in.nextLine(); // the n temperatures expressed as integers ranging from -273 to 552
        
        int n = 5;
        String temps = "";
        if(n > 0){
            int tempIntergers [] = new int [n];
            int count  = 0;
            for(String temp : temps.split(" ")){
                tempIntergers[count++] = Integer.parseInt(temp);
            }
       
            System.out.println(findClosestZero(tempIntergers));

        }
        else{
             System.out.println(0);
        }
        
    }
}
