/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author loc
 */
public class RotateLeftArray {

   
     public static int[] arrayLeftRotation(int[] array, int n, int k) {
        // copy array
        int temp[] = Arrays.copyOf(array, k);
       
        for ( int i = 0;i<n;i++){
            if(i + k >= n)
                break;
            array[i]= array[i + k];     
        }
        
        int count = 0;
        for (int i = n - k ; i<n;i++ ){
            array[i] = temp[count++];
        }
        
        return array;
    }
    
 
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int a[] = new int[n];
        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }
      
        int[] output = new int[n];
        output = arrayLeftRotation(a, n, k);
        for(int i = 0; i < n; i++)
            System.out.print(output[i] + " ");
      
        System.out.println();
      
    }
}
