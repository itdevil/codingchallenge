/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.linetest;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author loc
 */
public class SolutionTwo {

    static int min(int x, int y) {
        if (x < y) {
            return x;
        } else {
            return y;
        }
    }

    static int max(int x, int y) {
        if (x > y) {
            return x;
        } else {
            return y;
        }
    }

    static int solution(int n) {
        int countOne = 0;
        for (int i = 1; i <= n; i = i * 10) {
            int divider = i * 10;
            countOne = countOne + (n / divider) * i + min(max(n % divider - i + 1, 0), i);
        }
        return countOne;
    }

    static public void main(String[] args) throws FileNotFoundException {

        String testCases = "C:\\Users\\loc\\Desktop\\Solution_2_test_cases.txt";
        File file = new File(testCases);

        Scanner in = new Scanner(file);
        int n = in.nextInt(); // Number of arrays
        for (int i = 0; i < n; i++) {
            Integer positiveNumber = in.nextInt();
            Integer expectedResult = in.nextInt();
            if (solution(positiveNumber) == expectedResult) {
                System.out.println(String.format("Test Case %s passed ", i));
            } else {
                System.out.println(String.format("Test Case %s failed ", i));
            }

        }

    }
}
