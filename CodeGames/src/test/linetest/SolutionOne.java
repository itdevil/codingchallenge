/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.linetest;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 *
 * @author loc
 */
class SolutionOne {

    static int solution(int[] A, int K) {
        int n = A.length;
        if (K >= n) {
            return n;
        }
        int best = 1;
        int count = 1;
        for (int i = 0; i < n - K - 1; i++) {
            if (A[i] == A[i + 1]) {
                count = count + 1;
            } else {
                count = 1;
            }
            if (count > best) {
                best = count;
            }
        }
        int result = best + K;
        return result;

    }

    static public void main(String[] args) throws FileNotFoundException {

        String testCases = "C:\\Users\\loc\\Desktop\\Solution_1_test_cases.txt";
        File file = new File(testCases);

        Scanner in = new Scanner(file);
        int n = in.nextInt(); // Number of arrays
        for (int i = 0; i < n; i++) {
            String array[] = in.next().split(",");
            Integer k = in.nextInt();
            Integer expectedResult = in.nextInt();

            int arrayNumbers[] = Stream.of(array).mapToInt(Integer::parseInt).toArray();

            if (solution(arrayNumbers, k) == expectedResult) {
                System.out.println(String.format("Test Case %s passed ", i));
            } else {
                System.out.println(String.format("Test Case %s failed ", i));
            }

        }

    }
}
