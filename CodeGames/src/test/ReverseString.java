/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

/**
 *
 * @author loc
 */
public class ReverseString {
    
        public static void main(String []args) {

        String text =  "I am a boy";

        if( text.trim().equals("")){
            System.out.println(" ");
        }else{
            System.out.println(reverseString(text));
        }

    }

    public static String reverseString(String text){
        String [] words = text.trim().split(" ");
        StringBuilder reversedText = new StringBuilder();
        for(int i = words.length - 1;i>=0;i--){

            reversedText.append(words[i]).append(" ");
        }
        reversedText.deleteCharAt(reversedText.lastIndexOf(" "));
        return reversedText.toString();

    }
    
}
