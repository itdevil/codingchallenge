/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.Collections;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Stack;

/**
 *
 * @author loc
 */
public class BalanceBrackets {

    public static Map<Character, Character> brackets = new HashMap<>();

    public static boolean isLeftBracket(Character ch) {

        return ch.equals('{') || ch.equals('[') || ch.equals('(');

    }

    public static boolean isBalanced(String expression) {

        // create stact left brackets
        char[] test_brackets = expression.toCharArray();
        Stack<Character> temp = new Stack<>();
        
        temp.removeAllElements();
        for (char ch : test_brackets) {
            if (isLeftBracket(ch)) {
                temp.addElement(ch);
            } else {
                if (temp.isEmpty()) {
                    return false;
                } else {
                    Character left = temp.peek();
                    if (brackets.get(left).equals(ch)) {
                        temp.pop();
                    } else {
                        return false;
                    }
                }

            }
        }
        
       return temp.isEmpty();
    }

    public static void main(String[] args) {
//        Scanner in = new Scanner(System.in);
//        int t = in.nextInt();
//        for (int a0 = 0; a0 < t; a0++) {
//            String expression = in.next();

//5
//}][}}(}][))]
//[](){()}
//()
//({}([][]))[]()
//{)[](}]}]}))}(())(
        brackets.put('{', '}');
        brackets.put('(', ')');
        brackets.put('[', ']');

        System.out.println((isBalanced("[{}]")) ? "YES" : "NO");
//        }
//    }

    }

}
