/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

/**
 *
 * @author loc
 */
public class FindMaxNumber {
    
    public static int findIndexOfMaxElement(int [] mountains) {
        
        int maxH = mountains[0];
        int index = 0;
        for(int i = 0; i< mountains.length;i++) {
            
            if( mountains[i] >= maxH) {
                maxH =  mountains[i];
                index = i;
            }
        }
        
        return index;
        
    }
    
    public static void main(String args[]) {

            
        int [] mountains = new int[]{2,4,5,3,7,9,2,3};
        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println(findIndexOfMaxElement(mountains) ); // The index of the mountain to fire on.
        
    }
    
}
