/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

/**
 *
 * @author loc
 */
public class BitProgramming {
    
    public static void main(String agrs[]){
      
      int a = 10;	/* 10 = 1010 */
      int b = 8;	/* 8 = 1000 */
      int c = 0;

      c = a & b;        /* 8 = 1000 */
      System.out.println("a & b = " + c );

      c = a | b;        /* 10 = 1010 */
      System.out.println("a | b = " + c );

      c = a ^ b;        /* 2 = 0010 */
      System.out.println("a ^ b = " + c );

      c = ~a;           /*-61 = 1100 0011 */
      System.out.println("~a = " + c );

      c = a << 2;       /* 240 = 1111 0000 */
      System.out.println("a << 2 = " + c );

      c = a >> 2;       /* 15 = 1111 */
      System.out.println("a >> 2  = " + c );

      c = a >>> 2;      /* 15 = 0000 1111 */
      System.out.println("a >>> 2 = " + c );
    }
    
}
