package test;

import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");
/*


A binary gap within a positive integer N is any maximal sequence of consecutive zeros that is surrounded by ones at both ends in the binary representation of N.

For example, number 9 has binary representation 1001 and contains a binary gap of length 2. 
The number 529 has binary representation 1000010001 and contains two binary gaps: one of length 4 and one of length 3. 
The number 20 has binary representation 10100 and contains one binary gap of length 1.
The number 15 has binary representation 1111 and has no binary gaps.

Write a function:

    class Solution { public int solution(int N); }

that, given a positive integer N, returns the length of its longest binary gap. The function should return 0 if N doesn't contain a binary gap.

For example, given N = 1041 the function should return 5, because N has binary representation 10000010001 and so its longest binary gap is of length 5.

Assume that:

        N is an integer within the range [1..2,147,483,647].

Complexity:

        expected worst-case time complexity is O(log(N));
        expected worst-case space complexity is O(1).


*/
class BinaryGap {
    
    public int longestNumber = 0;
    
    public void updateLongestGap(ArrayList<Integer> longestBinaryGap ) {
        
        if(!longestBinaryGap.isEmpty()) {
            
            if (longestBinaryGap.get(0) == 1) 
            {
                int size = longestBinaryGap.size() - 1;
                if(size > longestNumber) {
                    longestNumber = size;
                }
            }
            
            longestBinaryGap.clear();
        }
    }
    public int solution(int N) {
        // write your code in Java SE 8
        
        ArrayList<Integer> longestBinaryGap = new ArrayList<>();
        int tmp = N;
        
        if(tmp == 1 || tmp == 0 ){
            return 0; 
        }
        
        int binary = 0;
        while(tmp != 0 ) {
            
            binary = tmp % 2;
           
            if(binary == 1) {
                updateLongestGap(longestBinaryGap);
            }
            longestBinaryGap.add(binary);
            
            tmp =  tmp / 2;
            
        }
        
        return longestNumber;
    }
}
