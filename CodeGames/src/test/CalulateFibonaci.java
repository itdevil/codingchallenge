/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

/**
 *
 * @author loc
 */
public class CalulateFibonaci {
    
     /*
     Finbonaci
      n = 1, F(n) = 1
      n = 2, F(n) = 1
      n > 2, F(n) = F(n-1) + F(n-2)
    
     calculate f(n) ?
    */
    
    public static int recursionCalF(int n)
    {
        if( n <=0 )
            return 0;
        else if( n == 1 || n == 2){
            return 1;
        }else{
         return recursionCalF(n - 1) + recursionCalF(n - 2);
        }
    }
    
    public static int calF(int n) {
        
        if( n <=0 )
            return 0;
        else if( n == 1 || n == 2)
            return 1;
            
        int f1 = 1;
        int f2 = 1;
        int fn = 0;
        for(int i = 3;i <= n;i++){
            fn = f1 + f2;
            f2 = f1;
            f1 = fn;
            
        }
        
        return fn;
    }
     public static void main(String[] args) {
    
       int n = 6;
        
       System.out.println(recursionCalF(n));  
       System.out.println(calF(n));  
    }
    
    
}
