/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import static test.ReverseString.reverseString;

/**
 *
 * @author loc
 */
public class MoveSelectElementToHead {
    
    public static List<Integer> moveSelectElementToHead(List<Integer> list,Integer selectedNumber){
        
        Iterator<Integer> iter = list.iterator();
        int countRemove = 0;
        while( iter.hasNext()){
            Integer number = iter.next();
            if(number.equals(selectedNumber)){
                iter.remove();
                countRemove++;
            }
        }
        
        for(int i = 0; i < countRemove;i++)
            list.add(0,selectedNumber);
        
        return list;    
    }
    
     public static void main(String []args) {

        Integer [] arrays = new Integer[]{2,3,6,8,9,3,5};       
        List<Integer> list = new LinkedList<>(Arrays.asList(arrays));        
        System.out.println(moveSelectElementToHead(list,9));
         
     }

}
