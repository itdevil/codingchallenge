/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.awt.RenderingHints;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import static test.MarkingAnagrams.numberNeeded;

/**
 *
 * @author loc
 */
public class RamsonNote {

    public static String checkCanUseMagaZine(String[] magazine, String[] ramsonNote) {

        Map<String,Integer> mapMagazine = countCharacters(magazine);
        
        Map<String,Integer> mapRamsonNote = countCharacters(ramsonNote);
        
        Set<String> keysInRamsonNote = mapRamsonNote.keySet();
        for(String key : keysInRamsonNote){
            Integer countWordInMagazine = mapMagazine.getOrDefault(key, 0);
            Integer countWordRamsonNote = mapRamsonNote.getOrDefault(key, 0);
            System.out.println(key + " " + countWordRamsonNote + " " + countWordInMagazine);
            if(countWordRamsonNote > countWordInMagazine){
                return "No";
            }
            
        }
        
        return "Yes";
    }

    public static Map<String, Integer> countCharacters(String[] text) {

        Map<String, Integer> mapCharacter = new HashMap<>();
        for (String word : text) {
            if (mapCharacter.containsKey(word)) {
                int count = mapCharacter.get(word) + 1;
                mapCharacter.put(word, count);
            } else {
                mapCharacter.put(word, 1);
            }
        }

        return mapCharacter;

    }

    public static void main(String[] args) {

        String magazine = "dlji eeyfb ox ayw fmphg x ffbkr z qiq vwvt zqgq nmw hlv c looms ffzif wfzx gzmf ez acidl mxwcw cm ichsi p pltu y jn re enujc ztm k s pkv hv om bsvw czy yzm lpli rj rm waqyk hkxf uffv rctam gp c enbez ngrc uos wfzx z hv acidl mxwcw hlv dlji enujc mxwcw cm p om pkv om x ox enbez pkv enujc rm ngrc x uos dlji zqgq c z rm eeyfb bsvw c dlji gzmf looms re p mxwcw gzmf hlv hv enbez y pkv rm y p gzmf ngrc gzmf wfzx";
        String ramsonnote = "ichsi c c uffv cm jn uffv s rm om uos czy czy nmw hv om gzmf ox uos pltu qiq czy rj vwvt s c ox bsvw acidl ffbkr ez looms ngrc yzm rj dlji wfzx waqyk om looms z om waqyk zqgq wfzx om dlji z nmw mxwcw";

        System.out.println(checkCanUseMagaZine(magazine.split(" "), ramsonnote.split(" ")));
    }
}
